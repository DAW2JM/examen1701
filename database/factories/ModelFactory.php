<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'created_at' => $faker->dateTime()
    ];
});


$factory->define(App\Ingredient::class, function () {
    $faker = Faker\Factory::create('es_ES');
    return [
        // 'name' => implode(' ', $faker->words(3))
        'name' => $faker->sentence(2, true),
        'created_at' => $faker->dateTime()
    ];
});

$factory->define(App\Dish::class, function () {
    $faker = Faker\Factory::create('es_ES');
    return [
        'name' => $faker->sentence(2, true),
        'description' => $faker->paragraph(),
        // 'family_id' => rand(1, 4)
        'type_id' => App\Type::all()->random()->id,
        'user_id' => App\User::all()->random()->id,
        'created_at' => $faker->dateTime()
    ];
});
