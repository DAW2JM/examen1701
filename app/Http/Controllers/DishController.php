<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dish;
use App\User;
use App\Type;
use App\Ingredient;
use App\DishIngredient;

class DishController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dishes = Dish::paginate();
        //dd($dishes);

        return view ('dish.index',['dishes' => $dishes]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $types = Type::all();
        return view ('dish.create', ['users' => $users, 'types' => $types]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'description' => 'required|max:500',
        ]);
        $dish = new Dish();

        $dish->name =$request->name;
        $dish->description =$request->description;
        $dish->user_id =$request->user;
        $dish->type_id =$request->type;
        $dish->save();

        return redirect('/dish');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dishIngredients = DishIngredient::where('dish_id',$id)->get();
        
        $ingredients = [];
        foreach ($dishIngredients as $key => $value) {

            $ingredient = Ingredient::findOrFail($value->ingredient_id);   
            array_push($ingredients, $ingredient);
            
        }

        //dd($dishIngredients);
        return view('dish.show', ['dishIngredients'=>$dishIngredients,'ingredients' => $ingredients]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Dish::find($id)->delete();
        return redirect('/dish');
    }
}
