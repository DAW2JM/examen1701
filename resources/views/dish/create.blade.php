@extends('layouts.app')
@section('content')
<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			<form action="/dish" method="POST">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="name">Nombre de plato: </label>
					<input type="text" class="form-control" name="name" id="name" />
				</div>
				<div class="form-group">
					<label for="description">Descripcion: </label>
					<input type="text" class="form-control" name="description" id="description" />
				</div>
				<div class="form-group">
					<label for="user">Usuario: </label>
					<select id="user" name="user" class="form-control">
						@foreach ($users as $user)
							<option 
								value="{{$user->id}}" 
								{{ old('user') == $user->id ? 'selected' : '' }}>
								{{$user->name}}
							</option>
						@endforeach
					</select>
					
					
				</div>
				<div class="form-group">
					<label for="type">Tipo de plato: </label>
					<select id="type" name="type" class="form-control">
						@foreach ($types as $type)
							<option 
								value="{{$type->id}}" 
								{{ old('type') == $type->id ? 'selected' : '' }}>
								{{$type->name}}
							</option>
						@endforeach
					</select>
					
				</div>
				<input type="submit" class="btn btn-primary"/>
			</form>
		</div>
	</div>
	
@endsection