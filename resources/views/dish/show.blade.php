@extends('layouts.app')
@section('content')

	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
	        <h1>Detalle del plato</h1>

	        <h3>Ingredientes</h3>
	        <ul class="list-group">
	        
	        @foreach ($ingredients as $key => $ingredient)
	        	<li class="list-group-item"><b>Nombre: </b>{{ $ingredient->name }} <b>Cantidad: </b> {{$dishIngredients[$key]->quantity}}</li>
	        	

	        @endforeach
	        </ul>
	    </div>
	</div>

@endsection('content')