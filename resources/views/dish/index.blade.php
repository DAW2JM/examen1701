@extends('layouts.app')
@section('content')
	<a href="/dish/create" class="btn btn-primary">Nuevo</a>
	<table class="table">
		<thead>
			<tr>
				<th>Id</th>
				<th>Nombre</th>
				<th>Descripcion</th>
				<th>Tipo de plato</th>
				<th>Usuario</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($dishes as $dish)
				<tr >
					<td>{{ $dish->id }}</td>
					<td>{{ $dish->name }}</td>
					<td>{{ $dish->description }}</td>
					<td>{{ $dish->type_id }}</td>
					<td>{{ $dish->user_id }}</td>
					
					<td>
						<form class="form-horizontal" action="/dish/{{$dish->id}}" method="POST">
							{{csrf_field()}}
							<input type="hidden" name="_method" value="DELETE"/>
							<a href="/dish/{{$dish->id}}" class="btn btn-info">Ver detalle</a>
							<input type="submit" class="btn btn-danger" value="Borrar" />
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<a href="/dish/create" class="btn btn-primary">Nuevo</a>

	<div class="col-xs-12 text-center">
		{{ $dishes->links() }}
	</div>
@endsection